#include <iostream> 
#include <fstream>
#include <sstream> 
#include <stdio.h>
#include <string> 
#include <iterator> 
#include <cwctype>
#include <functional>
#include <vector>

// Nuclear EOS includes
#include "NucEos.hh"

// SkyNet includes 
#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "Network/ReactionNetwork.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
#include "Network/NSE.hpp"
#include "Utilities/Constants.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"
#include "Utilities/BisectionMethod.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"

std::vector<double> ReadProfile(std::string fname, uint idx, double scale, 
    uint nvals) {
  if (idx>nvals-1) throw std::range_error("idx too big."); 
  std::vector<double> qq;
  
  std::ifstream file; 
  file.open(fname, std::ios::in); 
  std::string line; 
  while (std::getline(file, line)) {
    // Check if line contains a comment and remove portion of string after comment 
    std::size_t cidx = line.find_first_of('#');
    if (cidx != std::string::npos) line = line.substr(0, cidx-1); 
    
    // Read data from the line
    std::istringstream is(line); 
    std::vector<double> lvals {std::istream_iterator<double>(is), std::istream_iterator<double>()}; 
    if (lvals.size()==nvals) {
      qq.push_back(lvals[idx]*scale);
    }
    else if (cidx == std::string::npos) {
      std::cerr << "Line in file " << fname << " has wrong number of values." << std::endl;
      std::cerr << line << std::endl;
    }
  }
  file.close();
  return qq;
}

class NuclearEOS {
/*
Simple wrapper for the tabulated nuclear EOS 
*/
public: 
  NuclearEOS(std::string table_name = "./LSEOSF90_SLy4_rho391_temp163_ye65_version1.0_20160121.h5") :
      mEosTab(table_name, false)
  {}

  double CalcEntropyFromT9AndDensity(double T9, double rhogcc, double Ye) {
    
    double TMeV = T9*Constants::BoltzmannConstantInMeVPerGK;
    double eps, prs, ent, cs2, dedt, dpderho, dpdrhoe;
    double xa, xh, xn, xp, abar, zbar, mue, mun, mup, muhat; 
    int keyerr, anyerr;
    int npoints = 1;

    mEosTab.kt1_full(&npoints, &rhogcc, &TMeV, &Ye, 
      &eps, &prs, &ent, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &abar, &zbar, &mue, &mun, &mup, &muhat, 
      &keyerr, &anyerr);
    
    return ent;
  }

protected: 
  NucEos mEosTab;
};

class NSEEOS { 
/* A simple wrapper for the SkyNet equation of state, assuming full NSE 
   plus arbitrarily relativistic and degenerate electron/positron gas, and 
   photons. 
*/ 
public: 
  NSEEOS() : 
      nuclib(NuclideLibrary::CreateFromWebnucleoXML(
      SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml")),
      helm(SkyNetRoot + "/data/helm_table.dat"),
      screen(nuclib), 
      nse(nuclib, &helm, &screen) 
  {}
  
  NSEResult CalcFromEntropyAndDensity(double s, double rhogcc, double Ye) { 
      return nse.CalcFromEntropyAndDensity(s, rhogcc, Ye);
  }

protected: 

  NuclideLibrary nuclib; 
  HelmholtzEOS helm; 
  SkyNetScreening screen;
  NSE nse;
  
};

int main(int /*nargin*/, char** /*args*/) { 
  
  // Set up the two equations of state
  NuclearEOS nucEos;
  NSEEOS nseEos;  
  

  // Read in the profile 
  auto T9Arr = ReadProfile("particle7215", 2, 1.0/Constants::BoltzmannConstantInMeVPerGK, 19);
  auto rhoArr = ReadProfile("particle7215", 3, 1.0, 19); 
  auto YeArr = ReadProfile("particle7215", 5, 1.0, 19);
  auto sArr = ReadProfile("particle7215", 6, 1.0, 19); 
  
  // Loop over densities and compare temperatures for a fixed entropy, density, 
  // and Ye 
  //double T9 = 9.0; // Fix temperature at network starting temperature 

   
  FILE* fp; 
  fp = fopen("TemperatureCompare.out","w"); 

  fprintf(fp, "# rho (g/cc)         Ye       T9 Nuc       T9 NSE      Entropy  Entropy Orig\n");
  //for (double Ye = 0.05; Ye<=0.55; Ye+=0.05) {
  //  for (double rhogcc = 1.e6; rhogcc<1.e9; rhogcc*=1.1) {  
  for (uint i=0; i<T9Arr.size(); ++i) {
      double T9 = T9Arr[i];
      double rhogcc = rhoArr[i];
      double Ye = YeArr[i];
      double sCode = sArr[i];
      double s = nucEos.CalcEntropyFromT9AndDensity(T9, rhogcc, Ye); 
      try {
        auto nseResult = nseEos.CalcFromEntropyAndDensity(s, rhogcc, Ye);
        fprintf(fp, "%10e %10f %10e %10e %10e %10e\n", rhogcc, Ye, T9, nseResult.T9(), s, sCode);
      } catch(...) {
        fprintf(fp, "%10e %10f %10e    failed    %10e %10e\n", rhogcc, Ye, T9, s, sCode);
      }
  }
  
  fclose(fp); 

  return 0;
  
}