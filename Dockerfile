FROM debian:bullseye-20190708-slim as dev-base
LABEL author="Luke Roberts" 

# Install the required libraries
ARG DEBIAN_FRONTEND=noninteractive
RUN  apt-get update && apt-get install -y \
    build-essential \
    gfortran \ 
    gdb \ 
    gdbserver \
    make \
    cmake \
    git \
    curl \
    bzip2 \
    libhdf5-dev \
    liblapack-dev \
    libboost-all-dev \  
    libgsl-dev 

RUN export HDF5_DIR=/usr/lib/aarch64-linux-gnu/hdf5/serial/
#RUN curl https://stellarcollapse.org/EOS/SLy4_3335_rho391_temp163_ye66.h5.bz2 -o /SLy4_3335_rho391_temp163_ye66.h5.bz2 && bunzip2 /SLy4_3335_rho391_temp163_ye66.h5.bz2 
