cmake_minimum_required(VERSION 2.8.8 FATAL_ERROR)

enable_testing()

# Use absolute paths for libraries, needed on ICER
cmake_policy(SET CMP0060 NEW)

# set Release build type as default
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING
    "Configuration type (one of Debug, RelWithDebInfo, Release, MinSizeRel)"
    FORCE)
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules/")

project(EOSConvert CXX C Fortran)

set(CMAKE_EXPORT_COMPILE_COMMANDS "ON")

# enable warnings
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

# enable C++11
set (CMAKE_CXX_STANDARD 11)

# generate debugging symbols for release and debug
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -finstrument-functions")
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -g -finstrument-functions")
set(CMAKE_CXX_LINK_FLAGS "${CMAKE_CXX_LINK_FLAGS} -pg -lgfortran")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -funroll-loops")
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -O3 -funroll-loops")

# explicitly set DEBUG flag in Debug mode
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")

# to make shared libraries we need position independent code
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -fPIC")

# offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR include CACHE PATH "Installation directory for header files")
set(INSTALL_EXAMPLES_DIR examples CACHE PATH "Installation directory for examples")
set(INSTALL_DATA_DIR data CACHE PATH "Installation directory for data")

# External library dependencies
# Include nuc_eos
#find_library(NUCEOS_LIBRARY NAMES nuc_eos)
#if (EXISTS ${NUCEOS_LIBRARY})
#  set(EOSConversion_EXTERNAL_LIBS "${EOSConversion_EXTERNAL_LIBS};${NUCEOS_LIBRARY}") 
#  get_filename_component(NUCEOS_BASE ${NUCEOS_LIBRARY} DIRECTORY)
#  get_filename_component(NUCEOS_BASE ${NUCEOS_BASE} DIRECTORY)
#  include_directories(${NUCEOS_BASE}/include/)
#  message(STATUS "EOSDriver libraries found.")
#else()
#  message(STATUS "EOSDriver libraries not found.")
#endif() 

# Include SkyNet 
find_package(SkyNet REQUIRED)
set(EOSConversion_EXTERNAL_LIBS "${EOSConversion_EXTERNAL_LIBS};${SKYNET_LIBRARIES}") 

# Include EOSDriverCXX
find_package(EOSDriverCXX REQUIRED)
set(EOSConversion_EXTERNAL_LIBS "${EOSConversion_EXTERNAL_LIBS};nuc_eos") 

# Include HDF5
set(HDF5_USE_STATIC_LIBRARIES 1)
find_package(HDF5 REQUIRED COMPONENTS C CXX)
set(EOSConversion_EXTERNAL_LIBS "${EOSConversion_EXTERNAL_LIBS};${HDF5_LIBRARIES}")
include_directories(${HDF5_INCLUDE_DIRS})
link_directories(${HDF5_LIBRARY_DIRS})
message(STATUS "HDF5 version ${HDF5_VERSION}")

find_package(Boost REQUIRED system filesystem serialization)
set(EOSConversion_EXTERNAL_LIBS "${EOSConversion_EXTERNAL_LIBS};${Boost_LIBRARIES}")
include_directories(${Boost_INCLUDE_DIRS})

# add the source directory to the include list
add_executable(EOSConversion EOSConversion.cpp) 
target_link_libraries(EOSConversion ${EOSConversion_EXTERNAL_LIBS})
file(COPY particle7215 DESTINATION ${CMAKE_BINARY_DIR})

add_executable(DynamicBetaEquilibrium dynamicbetaequilibrium.cpp)
target_link_libraries(DynamicBetaEquilibrium ${EOSConversion_EXTERNAL_LIBS})

# Write out configuration
message(STATUS "")
message(STATUS "")
message(STATUS "Configuration options:")
message(STATUS "")
message(STATUS "              CMAKE_CXX_FLAGS: " ${CMAKE_CXX_FLAGS})
message(STATUS "         CMAKE_CXX_LINK_FLAGS: " ${CMAKE_CXX_LINK_FLAGS})
message(STATUS "        CMAKE_CXX_FLAGS_DEBUG: " ${CMAKE_CXX_FLAGS_DEBUG})
message(STATUS "      CMAKE_CXX_FLAGS_RELEASE: " ${CMAKE_CXX_FLAGS_RELEASE})
message(STATUS "          CMAKE_Fortran_FLAGS: " ${CMAKE_Fortran_FLAGS})
message(STATUS "     CMAKE_Fortran_LINK_FLAGS: " ${CMAKE_Fortran_LINK_FLAGS})
message(STATUS "    CMAKE_Fortran_FLAGS_DEBUG: " ${CMAKE_Fortran_FLAGS_DEBUG})
message(STATUS "  CMAKE_Fortran_FLAGS_RELEASE: " ${CMAKE_Fortran_FLAGS_RELEASE})

message(STATUS "")
message(STATUS "")


