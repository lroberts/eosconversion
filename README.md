

### Running in a docker container

You don't necessarily have to, but it is easy to get the correct libraries, etc. installed just by using the Dockerfile in the repository.

```bash
cd the_location_of_the_repository_on_your_computer
docker build -t eosconversion .
docker run -it -v "$(pwd)":/eosconversion eosconversion
```

### Build and install SkyNet

```bash 
cd /eosconversion/skynet/
mkdir build 
cd build 
cmake -DSKYNET_MATRIX_SOLVER=lapack -DCMAKE_INSTALL_PREFIX=/usr/local/ ..
make -j4 install
make test  
```

### Build and install EOSDriverCXX

```bash
cd ../../eosdrivercxx/ 
mkdir build 
cd build 
cmake .. 
make -j4 install 
```

### Build EOSConversion

EOSConversion links against SkyNet and EOSDriverCXX. If running in a docker container, this should work pretty much automatically. If not running in a docker container, you will have to change the CMAKE_PREFIX_PATH option to point towards where the EOSDriverCXX CMake config file is located. 

```bash
cd ../../
mkdir build 
cd build 
cmake -DCMAKE_PREFIX_PATH=/usr/local/include/ ..
make -j4 
```

This should create an executable in the build directory that can be run using 

```bash
./EOSConversion 
```

