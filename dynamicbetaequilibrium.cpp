/// \file r-process.cpp
/// \author jlippuner
/// \since Sep 3, 2014
///
/// \brief
///
///

#include <math.h>
#include <memory> 
#include <algorithm> 
#include <iostream> 
#include <string> 
#include <fstream> 
#include <sstream> 
#include <stdio.h>

#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "Network/NSEOptions.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
//#include "Reactions/SpecialReactionLibrary.hpp"
#include "Reactions/NeutrinoReactionLibrary.hpp"
#include "Reactions/ReactionPostProcess.hpp" 
#include "Utilities/FunctionVsTime.hpp" 
#include "Utilities/BisectionMethod.hpp" 
#include "EquationsOfState/NeutrinoHistoryBlackBody.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"
#include "OneDimensionalRoot.hpp"

class DynamicBetaEquilibrium { 
public: 
  DynamicBetaEquilibrium(NuclideLibrary* nuclib, EOS* eos, 
      ReactionLibraryBase* rlib) : 
    mpNuclib(nuclib), mpEos(eos), mpRlib(rlib) {} 

  double GetYeDot(double T9, double rho, double Ye, 
      std::shared_ptr<NeutrinoDistribution> nuDist) {
    NSEOptions opts;
    opts.DoScreening = false;  
    NSE nse(*mpNuclib, mpEos, nullptr, opts); 
    auto nseResult = nse.CalcFromTemperatureAndDensity(T9, rho, Ye);
    ThermodynamicState state = 
        mpEos->FromTAndRho(T9, rho, nseResult.Y(), *mpNuclib, nuDist);
    mpRlib->CalculateRates(state, 
      mpNuclib->PartitionFunctionsWithoutSpinTerm(T9), 100.0, nullptr, nullptr);
    std::vector<double> Ydot(nseResult.Y().size(), 0.0); 
    mpRlib->AddYdotContributions(nseResult.Y(), &Ydot); 
    double YeDot = 0.0; 
    for (unsigned int i=0; i<Ydot.size(); ++i) YeDot += Ydot[i]*mpNuclib->Zs()[i];
    return -YeDot;
  }

  double operator() (double T9, double rho,
      std::shared_ptr<NeutrinoDistribution> nuDist = 
      std::shared_ptr<NeutrinoDistribution>(new DummyNeutrinoDistribution())) {
    OneDimensionalRoot rootFind;
    auto root = rootFind(
        [this, rho, T9, nuDist](double Ye){return GetYeDot(T9, rho, Ye, nuDist);}, 
        0.01, 0.9);
    return root;
  }

protected: 
  NuclideLibrary* mpNuclib;
  EOS* mpEos; 
  ReactionLibraryBase* mpRlib; 

}; 

class ParticleData {
public: 
  ParticleData(std::string basename) {
    std::string line;
    double m, dt, time, T, rho, ye, e, rad, s, edot;
    
    // Read in the radius evolution
    std::ifstream f(basename); 
    while (std::getline(f, line)) {
      if (line[0] == '#') continue;
      std::stringstream ss(line);
      ss >> m >> time >> T >> rho >> e >> ye >> rad;
      radii.push_back(rad*1.48e5);
      rtimes.push_back(time);
    }
    f.close(); 
    auto plc = PowerLawContinuation(PiecewiseLinearFunction(rtimes, radii, false), 
        1.0, 0.01); 
    rtimes.push_back(1.e8); 
    radii.push_back(plc(1.e8));

    // Read in the temperature density evolution 
    std::ifstream f2(basename + ".h5.dat"); 
    while (std::getline(f2, line)) {
      if (line[0] == '#') continue;
      std::stringstream ss(line);
      ss >> time >> dt >> T >> rho >> s >> edot >> ye;
      times.push_back(time);
      T9s.push_back(T);
      rhos.push_back(rho);
      yes.push_back(ye);
    } 
    f2.close(); 


  }

  std::vector<double> rtimes, radii; 
  std::vector<double> times, T9s, rhos, yes; 
}; 

void CalculateParticle(const std::string particle, const double Lnu, 
    NuclideLibrary& nuclib, HelmholtzEOS& helm, 
    NeutrinoReactionLibrary& neutrinoLibrary) {

  // Read in the particle data 
  ParticleData p(particle); 
  
  // Build the time dependent neutrino distribution function
  double enue = 10.0;
  double enueb = 14.0;
  double lnue = Lnu;
  double lnueb = Lnu;
  auto nuHist = NeutrinoHistoryBlackBody::CreateConstant(p.rtimes, p.radii, 
      {NeutrinoSpecies::NuE, NeutrinoSpecies::AntiNuE}, 
      {enue/(3.15*Constants::BoltzmannConstantInMeVPerGK), 
       enueb/(3.15*Constants::BoltzmannConstantInMeVPerGK)},
      {0.0, 0.0}, // Neutrino degeneracy factors
      {lnue, lnueb}, false);
    
  //Calculate neutrino free dynamic beta equilibrium 
  FILE * fbeta = fopen((particle + ".betaeq").c_str(), "w");   
  fprintf(fbeta, "# [1] Time \n");
  fprintf(fbeta, "# [2] rho (g/cc) \n");
  fprintf(fbeta, "# [3] T9 (GK) \n");
  fprintf(fbeta, "# [4] ye actual \n");
  fprintf(fbeta, "# [5] ye equilibrium \n");
  int idx = 6;
  for (unsigned int i=0; i<neutrinoLibrary.Reactions().size(); ++i) {
    auto reac = neutrinoLibrary.Reactions()[i];
    fprintf(fbeta, "# [%i] %s (at ye actual) \n", idx++, reac.String().c_str());  
    fprintf(fbeta, "# [%i] %s (at ye actual) \n", idx++, 
        reac.Inverse().String().c_str());
  }
  
  DynamicBetaEquilibrium beq(&nuclib, &helm, &neutrinoLibrary); 
  NSEOptions opts;
  opts.DoScreening = false;
  NSE nse(nuclib, &helm, nullptr, opts);

  for (unsigned int i=0; (p.times[i] < 2.0) && (p.T9s[i] > 5.0); i += 20) { 
    double yeeq = beq(p.T9s[i], p.rhos[i], nuHist(p.times[i]));
    
    auto nseResult = nse.CalcFromTemperatureAndDensity(p.T9s[i], p.rhos[i], 
        p.yes[i]);
    ThermodynamicState state = 
        helm.FromTAndRho(p.T9s[i], p.rhos[i], nseResult.Y(), 
        nuclib, nuHist(p.times[i]));
    neutrinoLibrary.CalculateRates(state, 
      nuclib.PartitionFunctionsWithoutSpinTerm(p.T9s[i]), 100.0, nullptr, nullptr);
    
    std::cout << p.times[i] << " " <<  p.rhos[i] << " " << p.T9s[i] << " " 
        << p.yes[i] << " " << yeeq << std::endl;
    fprintf(fbeta, "%e %e %e %e %e ", p.times[i], p.rhos[i], 
        p.T9s[i], p.yes[i], yeeq);
    for (unsigned int j=0; j<neutrinoLibrary.Rates().size(); ++j)
      fprintf(fbeta, "%e %e ", neutrinoLibrary.Rates()[j], 
          neutrinoLibrary.InverseRates()[j]);
    fprintf(fbeta, "\n");
  }
  fclose(fbeta);
}

int main(int narg, char** args) {
  
  std::string runIdent = "test";
  if (narg > 1) { 
    runIdent = args[1];
  }
   
  auto nuclib = NuclideLibrary::CreateFromWebnucleoXML(
      SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml");

  NetworkOptions opts;
  opts.ConvergenceCriterion = NetworkConvergenceCriterion::Mass;
  opts.MassDeviationThreshold = 1.0E-10;
  opts.IsSelfHeating = true;
  opts.NSEEvolutionMinT9 = 9.5; 

  HelmholtzEOS helm(SkyNetRoot + "/data/helm_table.dat");
   
  // add neutrino reactions, if desired (will need a neutrino distribution
  // function set with net.LoadNeutrinoHistory(...))
  NeutrinoReactionLibrary neutrinoLibrary(SkyNetRoot
     + "/data/neutrino_reactions.dat", "Neutrino interactions", nuclib, opts);
  
  
  // Read in the particle data 
  std::vector<std::string> particles = {
      "../particle9600l0/particle9600",
      "../particle9600l51/particle9600",
      "../particle9600l52/particle9600",
      "../particle9600l53/particle9600",
      "../particle2390l0/particle2390",
      "../particle2390l51/particle2390",
      "../particle2390l52/particle2390",
      "../particle2390l53/particle2390",
      "../particle34950l0/particle34950",
      "../particle34950l51/particle34950",
      "../particle34950l52/particle34950",
      "../particle34950l53/particle34950"};
  std::vector<double> lnus = {
      1.e-10, 1.e51, 1.e52, 1.e53,
      1.e-10, 1.e51, 1.e52, 1.e53,
      1.e-10, 1.e51, 1.e52, 1.e53};
  
  for (unsigned int i=0; i<particles.size(); ++i) 
    CalculateParticle(particles[i], lnus[i], nuclib, helm, neutrinoLibrary);
  
  return 0;

}

